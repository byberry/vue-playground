
Vue.component('tabs', {
	template: '\
		<div class="tabs">\
			<div class="tabs-bar">\
				<!-- 标签页标题,这里要用 v-for -->\
				<div \
					:class="tabCls(item)" \
					v-for="(item, index) in navList" \
					@click="handleChange(index)">\
					{{ item.label }}\
				</div>\
			</div>\
			<div class="tabs-content">\
				<!-- 这里的 slot 就是嵌套的 pane -->\
				<slot></slot>\
			</div>\
		</div>',
	props: {
		// 这里的 value 是为了可以使用 v-model
		value: {
			type: [String, Number]
		}
	},
	data: function(){
		return {
			// 用于渲染 tabs 的标题
			currentValue: this.value,
			navList: []
		}
	},
	methods: {
		tabCls: function(item){
			return [
				'tabs-tab',
				{
					// 给当前选中的 tab 加一个 class
					'tabs-tab-active': item.name === this.currentValue
				}
			]
		},
		handleChange: function(index){
			var nav = this.navList[index];
			var name = nav.name;
			// 改变当前选中的 tab, 并触发下面的 watch
			this.currentValue = name;
			// 更新 value
			this.$emit('input', name);
			this.$emit('on-click', name);
		},
		getTabs(){
			// 通过遍历子组件,得到所有的 pane 组件
			return this.$children.filter(function(item){
				return item.$options.name === 'pane';
			});
		},
		updateNav(){
			this.navList = [];
			// 设置对 this 的引用, 在 function 回调里, this 指向的并不是 Vue 实例
			var _this = this;
			
			this.getTabs().forEach(function(pane, index){
				_this.navList.push({
					label: pane.label,
					name: pane.name || index
				});
				// 如果没有给 pane 设置 name, 默认设置它的索引
				if(!pane.name) pane.name = index;
				// 设置当前选中的 tab 的索引,在后面介绍
				if(index===0){
					if(!_this.currentValue){
						_this.currentValue = pane.name || index;
					}
				}
			});
			this.updateStatus();
		},
		updateStatus(){
			var tabs = this.getTabs();
			var _this = this;
			
			tabs.forEach(function(tab){
				return tab.show = tab.name === _this.currentValue;
			})
		}
	},
	watch: {
		value: function(val){
			this.currentValue = val;
		},
		currentValue: function(){
			this.updateStatus();
		}
	}
})